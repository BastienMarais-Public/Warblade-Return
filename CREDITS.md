# Crédits 


Une partie des ressources graphiques et sonores proviennent de ce 
site : [https://opengameart.org/](https://opengameart.org/)


Auteurs des sons et musiques :
* David McKee (ViRiX) : soundcloud.com/virix
* Skorpio
* K.L.Jonasson, Winnipeg, Canada.
* FoxSynergy
* Dan Knoflicek
* spuispuin
* anton

Auteurs des images :
*  Aleksander Kowalczyk, Retrocade.net



---------

Autres musiques utilisées :

Titre:  Cascade  
Auteur: Kubbi  
Source: http://www.kubbimusic.com/  
Licence: https://creativecommons.org/licenses/by-sa/3.0/deed.fr  
Téléchargement (8MB): https://auboutdufil.com/?id=485  

Titre:  Reset  
Auteur: Jaunter  
Source: https://jaunter.bandcamp.com  
Licence: https://creativecommons.org/licenses/by/3.0/  
Téléchargement (6MB): https://auboutdufil.com/?id=497  