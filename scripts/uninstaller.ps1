﻿#--------------------------------------------------------------------
# Fichier :     uninstaller.ps1      
# Description : Script de désinstallation du jeu
# Usage :       Dans powershell ==> & 'uninstaller.ps1'
# Auteur :      Bastien Marais      
# Email :       marais.bas@gmail.com       
# Dernière modification : 20/04/2020
#--------------------------------------------------------------------

#--------------------------------------------------------------------
#                           PARAMETRES
#--------------------------------------------------------------------

# Est utilisé en mode script
$scriptMode = $TRUE

#--------------------------------------------------------------------
#                           MAIN
#--------------------------------------------------------------------

if($scriptMode){
    cd ..
}

Write-Host "installer.ps1" -ForegroundColor Magenta

# Récupération du chemin racine
$path = pwd

# Récupération du chemin vers le bureau
$desktop = (Get-Item env:\USERPROFILE).value
$desktop = "$desktop\Desktop"

# Récupération du chemin vers Program Files
$programFiles =  (Get-Item env:\ProgramFiles).value

# Chemin vers git
$git = "$path\git\bin\git.exe"

# Chemin vers le raccourci
$shortcutPath  = "$desktop\Warblade Return.lnk"

# Chemin vers le dossier d'exécution
$shortcutWorkingDirectoryPath = "$programFiles\Warblade-Return"

# Chemin vers la cible du raccourci
$shortcutTargetPath = "$shortcutWorkingDirectoryPath\Warblade Return.exe"

# Chemin vers l'icone du raccourci
$shortcutIconPath = "$shortcutWorkingDirectoryPath\scripts\ps1ToExe\launcher.ico"

######## Désinstallation du jeu
Write-Host "| - Désinstallation du jeu" -ForegroundColor Green
Write-Host "| - | - Dossier du jeu : $shortcutWorkingDirectoryPath" -ForegroundColor Cyan

if(Test-Path -Path $shortcutWorkingDirectoryPath){
   
    Write-Host "| - | - Suppression en cours..." -ForegroundColor Cyan
    Remove-Item -Path $shortcutWorkingDirectoryPath -Recurse -Force
    Write-Host "| - | - Désinstallation terminée" -ForegroundColor Cyan
}
else {
   Write-Host "| - | - Dossier du jeu introuvable" -ForegroundColor Cyan
   Write-Host "| - | - Etape ignorée" -ForegroundColor Cyan
}

######## Suppression du raccourci
Write-Host "| - Suppression du raccourci" -ForegroundColor Green
Write-Host "| - | - Chemin vers le raccourci : $shortcutPath" -ForegroundColor Cyan
if((Test-Path -Path $shortcutPath)){
   Write-Host "| - | - Suppression du raccourci en cours..." -ForegroundColor Cyan
   Remove-Item -Path $shortcutPath -Force
   Write-Host "| - | - Suppression du raccourci terminée" -ForegroundColor Cyan
}
else {
   Write-Host "| - | - Raccourci introuvable" -ForegroundColor Cyan
   Write-Host "| - | - Etape ignorée" -ForegroundColor Cyan
}
cd $path