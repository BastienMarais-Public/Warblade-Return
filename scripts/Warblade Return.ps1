﻿#--------------------------------------------------------------------
# Fichier :     Warblade Return.ps1      
# Description : Script de lancement et mise a jour du programme
# Usage :       Dans powershell ==> &  'Warblade Return.ps1'
# Auteur :      Bastien Marais      
# Email :       marais.bas@gmail.com       
# Dernière modification : 12/12/2019
#--------------------------------------------------------------------


#--------------------------------------------------------------------
#                           PARAMETRES
#--------------------------------------------------------------------

# Nom du jar executable
$gameJarName = "Warblade Return.jar"

# Chemin vers le dossier contenant le jar executable
$gameFolder = "game"

# Est utilise en mode script
$scriptMode = $true


#--------------------------------------------------------------------
#                           FONCTIONS
#--------------------------------------------------------------------

# Fonction qui fait la maj du jeu
# Input : chemin vers la racine du repertoire
function updateGame(){
    param($homeFolderPath)
    $update = & "$homeFolderPath\scripts\git\bin\git.exe" pull
    if($update -eq "Already up to date."){
        Write-Host "| - | - Derniere version" -foreground cyan
    }
    else {
        Write-Host "| - | - Mise a jour effectuee" -foreground cyan
    }
}


# Fonction qui lance le jeu
# Inputs :
# - $gameJarName : nom du jar executable
# - $homeFolderPath : chemin vers la racine du jeu
# - $gameFolder : chemin vers le dossier game
# Output : null
function launchGame {
    param($gameJarName, $homeFolderPath, $gameFolder)
    $javaExe = "$homeFolderPath\jre\bin\java.exe"
    cd "$homeFolderPath\$gameFolder"
    Write-Host "| - Lancement du jeu" -foreground green
    Write-Host "| - | - Commande : & $javaExe -jar '$gameJarName'" -foreground cyan
    & "$javaExe" -jar "$gameJarName"
}


# Fonction main du programme
# Inputs :
# - $gameJarName : nom du jar executable 
# - $gameFolder : chemin vers le dossier game
# Output : null
function main {
    param($gameJarName, $gameFolder, $scriptMode)
    Write-Host "launch.ps1" -foreground magenta
    $homeFolderPath = pwd
    Write-Host "| - Recuperation du chemin complet vers le dossier contenant l'executable" -foreground green
    Write-Host "| - | - Chemin : $homeFolderPath" -foreground cyan
    Write-Host "| - Mises a jour" -foreground green
    updateGame -homeFolderPath $homeFolderPath
    launchGame -gameJarName $gameJarName -homeFolderPath $homeFolderPath -gameFolder $gameFolder
}

#--------------------------------------------------------------------
#                           MAIN
#--------------------------------------------------------------------

$pos = pwd
if($scriptMode){
    cd ..
}

main -gameJarName $gameJarName -gameFolder $gameFolder -scriptMode $scriptMode 
cd $pos