﻿##[Ps1 To Exe]
##
##Kd3HDZOFADWE8uO1
##Nc3NCtDXTlCDjr/U8TV+2W/hQX0ma8uPqoqAzZWAz//utyzada0VW1Z7ly35SXm4TecAW8kGt8MYRQk5E+IYs4bfPv+sea4Lh+18Y+Cc6LsqGjo=
##Kd3HFJGZHWLWoLaVvnQnhQ==
##LM/RF4eFHHGZ7/K1
##K8rLFtDXTiW5
##OsHQCZGeTiiZ4NI=
##OcrLFtDXTiW5
##LM/BD5WYTiiZ4tI=
##McvWDJ+OTiiZ4tI=
##OMvOC56PFnzN8u+Vs1Q=
##M9jHFoeYB2Hc8u+Vs1Q=
##PdrWFpmIG2HcofKIo2QX
##OMfRFJyLFzWE8uO1
##KsfMAp/KUzWI0g==
##OsfOAYaPHGbQvbyVvnQX
##LNzNAIWJGmPcoKHc7Do3uAuO
##LNzNAIWJGnvYv7eVvnRA5FnsTmkva4WpvKKy1o/c
##M9zLA5mED3nfu77Q7TV64AuzAl8qfMeXuLKihLO56//+tWvJWst0
##NcDWAYKED3nfu77Q7TV64AuzAl8qfMeXuLKihLO56//+tUU=
##OMvRB4KDHmHQvbyVvnRb5EXtR305LsGO+byi0cGL/vjutyTdTNomTEBihCec
##P8HPFJGEFzWE8pDU8CB+4EWub2k5b8yI2Q==
##KNzDAJWHD2fS8u+V1DVl50fvRm1rXMCPrKSppA==
##P8HSHYKDCX3N8u+VwTVk8ULrTCgGb9easKXH
##LNzLEpGeC3fMu77Ro2k3hQ==
##L97HB5mLAnfMu77Ro2k3hQ==
##P8HPCZWEGmaZ7/K1
##L8/UAdDXTlCDjr/U8TV+2W/hQX0ma8uPqoqAzZWAz//utyzada0VW1Z7ly35SXm4TecAW8kitcMTWRwuKrI556bFCennQLoP8g==
##Kc/BRM3KXxU=
##
##
##fd6a9f26a06ea3bc99616d4851b372ba
#--------------------------------------------------------------------
# Fichier :     Warblade Return.ps1      
# Description : Script de lancement et mise a jour du programme
# Usage :       Dans powershell ==> &  'Warblade Return.ps1'
# Auteur :      Bastien Marais      
# Email :       marais.bas@gmail.com       
# Dernière modification : 19/04/2020
#--------------------------------------------------------------------


#--------------------------------------------------------------------
#                           PARAMETRES
#--------------------------------------------------------------------

# Nom du jar executable
$gameJarName = "Warblade Return.jar"

# Chemin vers le dossier contenant le jar executable
$gameFolder = "game"

# Est utilise en mode script
$scriptMode = $false


#--------------------------------------------------------------------
#                           FONCTIONS
#--------------------------------------------------------------------

# Fonction qui fait la maj du jeu
# Input : chemin vers la racine du repertoire
function updateGame(){
    param($homeFolderPath)
    $update = & "$homeFolderPath\scripts\git\bin\git.exe" pull
    if($update -eq "Already up to date."){
        Write-Host "| - | - Derniere version" -foreground cyan
    }
    else {
        Write-Host "| - | - Mise a jour effectuee" -foreground cyan
    }
}


# Fonction qui lance le jeu
# Inputs :
# - $gameJarName : nom du jar executable
# - $homeFolderPath : chemin vers la racine du jeu
# - $gameFolder : chemin vers le dossier game
# Output : null
function launchGame {
    param($gameJarName, $homeFolderPath, $gameFolder)
    $javaExe = "$homeFolderPath\jre\bin\java.exe"
    cd "$homeFolderPath\$gameFolder"
    Write-Host "| - Lancement du jeu" -foreground green
    Write-Host "| - | - Commande : & $javaExe -jar '$gameJarName'" -foreground cyan
    & "$javaExe" -jar "$gameJarName"
}


# Fonction main du programme
# Inputs :
# - $gameJarName : nom du jar executable 
# - $gameFolder : chemin vers le dossier game
# Output : null
function main {
    param($gameJarName, $gameFolder, $scriptMode)
    Write-Host "Warblade Return.ps1" -foreground magenta
    $homeFolderPath = pwd
    Write-Host "| - Recuperation du chemin complet vers le dossier contenant l'executable" -foreground green
    Write-Host "| - | - Chemin : $homeFolderPath" -foreground cyan
    Write-Host "| - Mise a jour" -foreground green
    updateGame -homeFolderPath $homeFolderPath
    launchGame -gameJarName $gameJarName -homeFolderPath $homeFolderPath -gameFolder $gameFolder
}

#--------------------------------------------------------------------
#                           MAIN
#--------------------------------------------------------------------

$pos = pwd
if($scriptMode){
    cd ..
}

main -gameJarName $gameJarName -gameFolder $gameFolder -scriptMode $scriptMode 
cd $pos