﻿##[Ps1 To Exe]
##
##Kd3HDZOFADWE8uO1
##Nc3NCtDXTlCDjr/U8TV+2W/hQX0ma8uPqoqAzZWAz//utyzada0VW1Z7ly35SXm4TecAW8kGt8MYRQk5E+IYs4bfPv+seaADnK12aOru
##Kd3HFJGZHWLWoLaVvnQnhQ==
##LM/RF4eFHHGZ7/K1
##K8rLFtDXTiW5
##OsHQCZGeTiiZ4NI=
##OcrLFtDXTiW5
##LM/BD5WYTiiZ4tI=
##McvWDJ+OTiiZ4tI=
##OMvOC56PFnzN8u+Vs1Q=
##M9jHFoeYB2Hc8u+Vs1Q=
##PdrWFpmIG2HcofKIo2QX
##OMfRFJyLFzWE8uK1
##KsfMAp/KUzWI0g==
##OsfOAYaPHGbQvbyVvnQX
##LNzNAIWJGmPcoKHc7Do3uAuO
##LNzNAIWJGnvYv7eVvnRA5FnsTmkva4WpvKKy1o/c
##M9zLA5mED3nfu77Q7TV64AuzAn0lZ8uIrberyISusfr/6kU=
##NcDWAYKED3nfu77Q7TV64AuzAn0lZ8uIrberyISusfr/6kU=
##OMvRB4KDHmHQvbyVvnRTRoL9S2Y4esSXtbezwZSuv+75+y/cXNojSEZ1mij4AQuPXOYHR/t1
##P8HPFJGEFzWE8pDU8CB+4EWub2k5b8yI2Q==
##KNzDAJWHD2fS8u+V1DVl50fvRm1rXMCPrKSppA==
##P8HSHYKDCX3N8u+VwTVk8ULrTCgGb9easKXH
##LNzLEpGeC3fMu77Ro2k3hQ==
##L97HB5mLAnfMu77Ro2k3hQ==
##P8HPCZWEGmaZ7/K1
##L8/UAdDXTlCDjr/U8TV+2W/hQX0ma8uPqoqAzZWAz//utyzada0VW1Z7ly35SXm4TecAW8kAutgfRgkrI/4O8PzVA+LJ
##Kc/BRM3KXxU=
##
##
##fd6a9f26a06ea3bc99616d4851b372ba
#--------------------------------------------------------------------
# Fichier :     uninstaller.ps1      
# Description : Script de désinstallation du jeu
# Usage :       Dans powershell ==> & 'uninstaller.ps1'
# Auteur :      Bastien Marais      
# Email :       marais.bas@gmail.com       
# Dernière modification : 20/04/2020
#--------------------------------------------------------------------

#--------------------------------------------------------------------
#                           PARAMETRES
#--------------------------------------------------------------------

#--------------------------------------------------------------------
#                           MAIN
#--------------------------------------------------------------------

Write-Host "installer.ps1" -ForegroundColor Magenta

# Récupération du chemin vers le bureau
$desktop = (Get-Item env:\USERPROFILE).value
$desktop = "$desktop\Desktop"

# Récupération du chemin vers Program Files
$programFiles =  (Get-Item env:\ProgramFiles).value

# Chemin vers le raccourci
$shortcutPath  = "$desktop\Warblade Return.lnk"

# Chemin vers le dossier d'exécution
$shortcutWorkingDirectoryPath = "$programFiles\Warblade-Return"

# Chemin vers la cible du raccourci
$shortcutTargetPath = "$shortcutWorkingDirectoryPath\Warblade Return.exe"

# Chemin vers l'icone du raccourci
$shortcutIconPath = "$shortcutWorkingDirectoryPath\scripts\ps1ToExe\launcher.ico"

######## Désinstallation du jeu
Write-Host "| - Désinstallation du jeu" -ForegroundColor Green
Write-Host "| - | - Dossier du jeu : $shortcutWorkingDirectoryPath" -ForegroundColor Cyan

if(Test-Path -Path $shortcutWorkingDirectoryPath){
   
    Write-Host "| - | - Suppression en cours..." -ForegroundColor Cyan
    Remove-Item -Path $shortcutWorkingDirectoryPath -Recurse -Force 
    Write-Host "| - | - Désinstallation terminée" -ForegroundColor Cyan
}
else {
   Write-Host "| - | - Dossier du jeu introuvable" -ForegroundColor Cyan
   Write-Host "| - | - Etape ignorée" -ForegroundColor Cyan
}

######## Suppression du raccourci
Write-Host "| - Suppression du raccourci" -ForegroundColor Green
Write-Host "| - | - Chemin vers le raccourci : $shortcutPath" -ForegroundColor Cyan
if((Test-Path -Path $shortcutPath)){
   Write-Host "| - | - Suppression du raccourci en cours..." -ForegroundColor Cyan
   Remove-Item -Path $shortcutPath -Force
   Write-Host "| - | - Suppression du raccourci terminée" -ForegroundColor Cyan
}
else {
   Write-Host "| - | - Raccourci introuvable" -ForegroundColor Cyan
   Write-Host "| - | - Etape ignorée" -ForegroundColor Cyan
}