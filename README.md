# Warblade-Return

## Pour jouer :

### Lancement du jeu

Faites un double clique sur le fichier `Warblade Return.exe`

### Touches par défaut :


|       Action       | Touche J1 | Touche J2  |
| :----------------: | :-------: | :--------: |
| Déplacement gauche |     Q     |    LEFT    |
| Déplacement droit  |     D     |   RIGHT    |
|       Tirer        |   SPACE   | CTRL RIGHT |
|       Pause        |     P     |     P      |
|      Quitter       |   ECHAP   |   ECHAP    |


## Modifier les paramètres manuellement

Pour modifier les paramètres, il faut utiliser les fichiers suivants :
```sh
Warblade-Return
| - game
| - | - settings 
| - | - | - settings.txt  ==> Gestion de la résolution, difficulté et nombre de joueur (1 ou 2)
| - | - | - keys.txt ==> Association des actions a des touches
```

## Pour désinstaller le jeu

Copier le fichier `uninstaller.exe` dans vos documents ou sur votre bureau puis cliquez dessus.
Une fois qu'il a fini, supprimez le également.
